﻿using System;

namespace Ecuatia_de_gradul_2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Introduceti a: ");

            if (!double.TryParse(Console.ReadLine(), out double a))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            Console.Write("Introduceti b: ");

            if (!double.TryParse(Console.ReadLine(), out double b))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            Console.Write("Introduceti c: ");

            if (!double.TryParse(Console.ReadLine(), out double c))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            if (a != 0)
            {
                double delta = Math.Pow(b, 2) - 4 * a * c;

                if (delta >= 0)
                {
                    double x1 = (-b - Math.Sqrt(delta)) / 2 * a;
                    double x2 = (-b + Math.Sqrt(delta)) / 2 * a;

                    Console.WriteLine("Solutia ecuatiei x1={0}, x2={1}.", x1, x2);
                } 
                else
                {
                    Console.WriteLine("Solutii complexe");
                }
            }
            else
            {
                if (b!=0)
                {
                    Console.WriteLine("Solutia ecuatiei de gr1 este: " + (-c / b));
                }
                else
                {
                    Console.WriteLine("Ecuatie imposibila");
                }
            }

            Console.ReadKey();
        }
    }
}